<%-- 
    Document   : AtencionAlCliente
    Created on : 18/12/2018, 03:10:32 AM
    Author     : Erik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String sala = (String) getServletContext().getAttribute("sala");
    if (sala == null) {
        sala = "";
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Tienda de Electronicos</title>
        <%@include file="head.html" %>
        <style type="text/css">
            #sala {
                margin: auto;
                border: 1px solid #999;
                padding: 6px;
                overflow: auto;
                width: 100%;
                height: 400px;
            }
        </style>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <div class="container">

            <article>
                <header>
                    <div class="left">
                        <i class="fas fa-angle-right"></i><span>Chatea y pregrunta tus dudas al staff</span>
                    </div>

                    <div class="right">
                        Otros usuarios tambien te pueden ayudar.
                    </div>
                </header>
                <hr>
                <div id="sala" ><%=sala%></div>

                <div style="text-align: center;">
                    <input type="text" name="msg" style="width: 244px" id="msg"/>
                    <% if (loggedin){ %>
                    <button onclick="goAjaxChat();">Enviar</button>
                    <% } %>
                </div>

            </article>
            <article>
                <br>
                 <header>
                    <div class="left">
                        <i class="fas fa-angle-right"></i><span>Contactanos por Email</span>
                    </div>

                    <div class="right">
                        Te responderemos lo más pronto posible.
                    </div>
                </header>
                
                 <hr>
                <form class="form-horizontal">
                    <fieldset>
                        
                       

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-12 control-label" for="Nombre_Contacto">Nombre</label>  
                            <div class="col-md-12">
                                <input id="Nombre_Contacto" name="Nombre_Contacto" type="text" placeholder="Ingresa tu nombre" class="form-control input-md">

                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-12 control-label" for="email_Contacto">Email</label>  
                            <div class="col-md-12">
                                <input id="email_Contacto" name="email_Contacto" type="text" placeholder="" class="form-control input-md" required="">

                            </div>
                        </div>

                        <!-- Textarea -->
                        <div class="form-group">
                            <label class="col-md-12 control-label" for="mensaje">Mensaje</label>
                            <div class="col-md-12">                     
                                <textarea class="form-control" id="mensaje" name="mensaje"></textarea>
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-12 control-label" for="enviar"></label>
                            <div class="col-md-12">
                                <button id="enviar" name="enviar" class="btn btn-primary" onclick="goAjaxSendEmail();">Enviar</button>
                            </div>
                        </div>

                    </fieldset>
                </form>
            </article>
        </div>
        <jsp:include page="footer.html"/>
    </body>
</html>
