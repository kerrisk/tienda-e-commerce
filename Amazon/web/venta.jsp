<%-- 
    Document   : venta
    Created on : 18-dic-2018, 3:42:23
    Author     : Kristian
--%>

<%@page session="true"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Finalizar Venta</title>
        <%@include file="head.html" %>

    </head>
    <body class="">
        <%--<jsp:include page="header.jsp"/>--%>
        <%@include file="header.jsp" %>  
        <div class="container">

            <article>
                <header>
                    <div class="left">
                        <i class="fas fa-angle-right"></i><span style="font-size: 32px"> Finalizacion de la Compra</span>
                    </div>
                    <div class="right">
                        Ventas
                    </div>
                </header>
                <hr>
                <table>
                    <tr>
                        <th colspan="2">Producto</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                    </tr>
                    <%
                        for (int i = 0; i < carroTemp.getArticulos().size(); i++) {
                            product_temp = (Producto) carroTemp.getArticulos().get(i);
                            //System.out.println("1");
                    %>
                    <%
                        byte[] imageData = product_temp.getImagen();
                        String data = "data:image/jpeg;base64," + DatatypeConverter.printBase64Binary(imageData);

                    %>
                    <tr style="background-color: white; color: black;">
                        <td><%=product_temp.getDescripcion()%></td>
                        <td><img class="imgFin" src="<%=data%>" border=0/></td>
                        <td>Precio: $<%=product_temp.getPrecio()%></td>
                        <td>Cantidad: <%=product_temp.getCantidad()%></td>
                    </tr>
                    <%

                            //System.out.println("2");
                        }
                    %>
                    <hr>
                    <tr style="background-color: white; color: black;">
                        <td colspan="4">Subtotal: <%=carroTemp.getSubtotal()%></td>

                    </tr>
                    <%
                        //System.out.println("3");
                    %>
                </table>
                <hr>
                <form action="/Amazon/ProcesarCompra" method="POST">
                    <div class="form-group">
                        <label class="col-md-12 control-label">CUPONES DE DESCUENTO</label>
                        <div class="col-md-12">
                            <input type="text" placeholder="Ingrese Cupon">
                        </div>
                    </div>  
                    <hr class="linea">
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="direccion">Direccion</label>  
                        <div class="col-md-12">
                            <input id="direccion" name="direccion" type="text" placeholder="" class="form-control input-md" required="">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="ciudad">Ciudad</label>  
                        <div class="col-md-12">
                            <input id="ciudad" name="ciudad" type="text" placeholder="" class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="cp">Codigo Postal</label>  
                        <div class="col-md-12">
                            <input id="cp" name="cp" type="text" placeholder="" class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="telcel">Numero Telefonico</label>  
                        <div class="col-md-12">
                            <input id="telcel" name="telcel" type="text" placeholder="" class="form-control input-md">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="Id_Pais">PAIS A ENVIAR</label>
                        <div class="col-md-12">
                            <select id="Pais" name="Id_Pais" class="form-control">
                                next();
                                %>
                                <option value="0">Mexico</option>
                                <option value="1">USA</option>



                            </select>
                        </div>
                    </div>
                    <!-- Multiple Radios (inline) -->
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="formaPago">Forma de Pago</label>
                        <div class="col-md-12"> 
                            <label class="radio-inline" for="formaPago-0">
                                <input type="radio" name="formaPago" id="formaPago-0" value="Paypal" checked="checked">
                                Paypal
                            </label> 
                            <label class="radio-inline" for="formaPago-1">
                                <input type="radio" name="formaPago" id="formaPago-1" value="MercadoPago">
                                MercadoPago
                            </label> 
                            <label class="radio-inline" for="formaPago-2">
                                <input type="radio" name="formaPago" id="formaPago-2" value="AmazonCash">
                                AmazonCash
                            </label> 
                            <label class="radio-inline" for="formaPago-3">
                                <input type="radio" name="formaPago" id="formaPago-3" value="Credito/Debito">
                                Credito/Debito
                            </label>
                        </div>
                    </div>
                    <hr>
                    <input id="submitCompra" type="submit" name="submit" class="btn btn-primary" value="FINALIZAR COMPRA"></input


                </form>
            </article>
            <footer>
                <div class="left">
                    <a href="#"><i class="fas fa-rss-square"></i><span>Suscribete</span></a>
                    <a href="https://www.twitter.com"><i class="fab fa-twitter"></i><span>Siguenos en Twitter</span></a>
                    <a href="https://www.facebook.com"><i class="fab fa-facebook"></i></a>
                    <a href="https://www.google.com"><i class="fab fa-google-plus-g"></i></a>
                    <a href="https://www.blogger.com"><i class="fab fa-blogger"></i></a>

                </div>
                <div class="right">
                    <img src="images/1200px-Apple_logo_black.svg.png" alt="">
                    <img src="images/gentoo-g.png" alt="">
                    <img src="images/1280px-RedHat.svg.png" alt="">
                </div>
            </footer>	
        </div>
        <jsp:include page="footer.html"/>
    </body>
</html>
