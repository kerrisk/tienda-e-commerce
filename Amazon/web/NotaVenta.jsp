<%-- 
    Document   : NotaVenta
    Created on : 18/12/2018, 06:40:26 AM
    Author     : Erik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include file="head.html" %>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <div class="container">

            <article>
                <header>
                    <div class="left">
                        <i class="fas fa-angle-right"></i><span style="font-size: 32px"> Recibo</span>
                    </div>
                    <div class="right">
                        Esta Nota ha sido enviada a tu correo registrado.
                    </div>
                </header>
              
            </article>
            <%=request.getSession().getAttribute("NotaVenta")%>
            <footer>
                <div class="left">
                    <a href="#"><i class="fas fa-rss-square"></i><span>Suscribete</span></a>
                    <a href="https://www.twitter.com"><i class="fab fa-twitter"></i><span>Siguenos en Twitter</span></a>
                    <a href="https://www.facebook.com"><i class="fab fa-facebook"></i></a>
                    <a href="https://www.google.com"><i class="fab fa-google-plus-g"></i></a>
                    <a href="https://www.blogger.com"><i class="fab fa-blogger"></i></a>

                </div>
                <div class="right">
                    <img src="images/1200px-Apple_logo_black.svg.png" alt="">
                    <img src="images/gentoo-g.png" alt="">
                    <img src="images/1280px-RedHat.svg.png" alt="">
                </div>
            </footer>	
        </div>
        
       <jsp:include page="footer.html"/>
    </body>
</html>
