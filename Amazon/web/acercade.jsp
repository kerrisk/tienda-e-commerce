<%-- 
    Document   : acercade
    Created on : 17-dic-2018, 3:35:51
    Author     : Kristian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mision y Vision</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="js/ajaxCreateRequest.js" type="text/javascript"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
    </head>
    <body>
        <%@include file="header.jsp" %>
        <div class="container">
            <article>
                <header style="margin-top: 40px;">
                    <div class="left">
                        <i class="fas fa-angle-right"></i><span>Acerca de...</span>
                    </div>
                    <div class="right">
                        NOSOTROS
                    </div>
                </header>
                <hr>
                <div>
                    <p class="main">La tienda en linea mas centrada en el cliente del mundo</p>
                    <p class="sec">
                        Amazon es una de las 500 mayores empresas de EE.UU. La compañía, con sede en Seattle (Washington) es un líder global en el comercio electrónico. Desde que Jeff Bezos lanzó Amazon.com en 1995, se ha hecho un progreso significativo en la oferta, en los sitios web y en la red internacional de distribución y servicio al cliente. En la actualidad, Amazon ofrece gran variedad de productos, desde libros o productos electrónicos, hasta raquetas de tenis o diamantes. Tenemos una presencia directa en Estados Unidos, Reino Unido, Alemania, Francia, Italia, España, Japón, Canadá y China, pero además podemos servir a los clientes en la mayoría de los países del mundo.

                    </p>

                    <hr class="linea">
                </div>
                <div>
                    <p class="main">Innovación tecnológica</p>
                    <p class="sec">
                        La innovación tecnológica es la base para la expansión de Amazon y permite a los clientes tener más y más categorías de productos, adaptados a sus necesidades y a un precio más bajo. Ofrecemos experiencia de compra personalizada: por ejemplo, nuestros clientes pueden comprar rápidamente, con el "Pedido 1-Clic" y utilizar diversas herramientas, tales como consejos para descubrir nuevos productos.
                    </p>
                    
                    <hr class="linea">
                </div>
                 <div>
                    <p class="main">La plataforma de Amazon</p>
                    <p class="sec">
                       En 2000, Amazon comenzó a ofrecer su plataforma de comercio a otros minoristas y proveedores. Hoy en día, muchas empresas están trabajando con Amazon Services para desarrollar su oferta de comercio electrónico, de servicios técnicos y de servicio al cliente y la gestión y el envío de los pedidos. Las principales marcas también utilizan Amazon como canal de ventas, ofreciendo sus productos directamente a los sitios de Amazon. Por último, los desarrolladores externos de software utilizan Amazon Web Services para crear aplicaciones y servicios que se proporcionan a los clientes y vendedores de Amazon.
                    </p>
                    
                    <hr class="linea">
                </div>
                
                <div>
                    <img src="images/AWS-Progression-Inforgraphic-01.png" alt="">
                </div>
            </article>
            <footer>
                <div class="left">
                    <a href="#"><i class="fas fa-rss-square"></i><span>Suscribete</span></a>
                    <a href="https://www.twitter.com"><i class="fab fa-twitter"></i><span>Siguenos en Twitter</span></a>
                    <a href="https://www.facebook.com"><i class="fab fa-facebook"></i></a>
                    <a href="https://www.google.com"><i class="fab fa-google-plus-g"></i></a>
                    <a href="https://www.blogger.com"><i class="fab fa-blogger"></i></a>

                </div>
                <div class="right">
                    <img src="images/1200px-Apple_logo_black.svg.png" alt="">
                    <img src="images/gentoo-g.png" alt="">
                    <img src="images/1280px-RedHat.svg.png" alt="">
                </div>
            </footer>
        </div>
        <jsp:include page="footer.html"/>
    </body>
</html>

