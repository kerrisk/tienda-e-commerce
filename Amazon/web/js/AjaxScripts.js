/* 
 * Copyright (C) 2018 erikgoh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
function goAjaxAddCarrito( id_producto, cantidad) {
        
        var ajaxRequest = CreateRequest();
        if (ajaxRequest !== false) {
            ajaxRequest.onreadystatechange = function () {
                if (ajaxRequest.readyState === 4) {
                    if (ajaxRequest.status === 200) {
                        var jsonResponse = ajaxRequest.responseText;
                        console.log(jsonResponse);
                        //alert("Jsonresponse:"+jsonResponse);
                        var responseObj = JSON.parse(jsonResponse);
                        var salida = "";
                        if (responseObj.hasOwnProperty("Error")) {
                            alert(responseObj.Error);
                            
                        } else {
                            if (responseObj.producto_anadido !== false) {
                                salida=document.getElementById("carrito").innerHTML;
                                salida += "<tr style=\"background-color: white\; color: black\;\">\
                <td>"+responseObj.descripcion+"</td>\
                <td><img class=\"imgCar\"src=\"images/default.jpg\" border=0/></td>\
                <td><i class=\"fas fa-trash-alt\"></i></td>\
                </tr>\
                <tr>\
                <td>Precio: $"+responseObj.Precio+"</td>\
                <td>Cantidad: "+cantidad+"</td>\
                </tr>";
                                 alert("El producto se ha agregado(recarga la pagina para ver los cambios)");   
                
                            } 
                        }
                    }
                }
            };
            var id= encodeURIComponent(id_producto);
            var cant=encodeURIComponent(cantidad);
            ajaxRequest.open("POST", "/Amazon/AddProduct", true);
            ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajaxRequest.send("Id_Producto=" + id_producto + "&cantidad=" + cantidad);
        }
        
}

function goAjaxRemoveCarrito( i) {
        
        var ajaxRequest = CreateRequest();
        if (ajaxRequest !== false) {
            ajaxRequest.onreadystatechange = function () {
                if (ajaxRequest.readyState === 4) {
                    if (ajaxRequest.status === 200) {
                        var jsonResponse = ajaxRequest.responseText;
                        console.log(jsonResponse);
                        //alert("Jsonresponse:"+jsonResponse);
                        var responseObj = JSON.parse(jsonResponse);
                        var salida = "";
                        if (responseObj.hasOwnProperty("Error")) {
                            alert(responseObj.Error);
                            
                        } else {
                            if (responseObj.producto_eliminado !== false) {
                                   alert("El producto ha sido eliminado(recarga la pagina para ver los cambios)");                                                                                                    
                
                            } 
                        }
                    }
                }
            };
            var i= encodeURIComponent(i);
            ajaxRequest.open("POST", "/Amazon/RemoveProduct", true);
            ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajaxRequest.send("indice=" + i );
        }
        
}
function goAjaxDeleteProduct( id) {
        
        var ajaxRequest = CreateRequest();
        if (ajaxRequest !== false) {
            ajaxRequest.onreadystatechange = function () {
                if (ajaxRequest.readyState === 4) {
                    if (ajaxRequest.status === 200) {
                        var jsonResponse = ajaxRequest.responseText;
                        console.log(jsonResponse);
                        //alert("Jsonresponse:"+jsonResponse);
                        var responseObj = JSON.parse(jsonResponse);
                        var salida = "";
                        if (responseObj.hasOwnProperty("Error")) {
                            alert(responseObj.Error);
                            
                        } else {
                            if (responseObj.producto_eliminado !== false) {
                                   alert("El producto ha sido eliminado(recarga la pagina para ver los cambios)");                                                                                                    
                
                            } 
                        }
                    }
                }
            };
            var id= encodeURIComponent(id);
            ajaxRequest.open("POST", "/Amazon/DeleteProduct", true);
            ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajaxRequest.send("id=" + id);
        }
        
}

    

