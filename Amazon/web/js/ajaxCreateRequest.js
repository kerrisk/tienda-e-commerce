/* 
 * Copyright (C) 2018 erikgoh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* Check browser type and create ajaxRequest object
 Put this function in an external .js file and use it for your
 Ajax programs */
function CreateRequest() {
    var ajaxRequest; // The variable that makes Ajax possible!
    try {
// Opera 8.0+, Firefox, Safari
        ajaxRequest = new XMLHttpRequest(); // Create the object
    } catch (e) {
// Internet Explorer Browsers
        try {
            ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                return false;
            }
        }
    }
    return ajaxRequest;
} //End function