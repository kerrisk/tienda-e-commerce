/* 
 * Copyright (C) 2018 Erik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
function goAjaxChat() {
        
        var ajaxRequest = CreateRequest();
        if (ajaxRequest !== false) {
            ajaxRequest.onreadystatechange = function () {
                if (ajaxRequest.readyState === 4) {
                    if (ajaxRequest.status === 200) {
                        var jsonResponse = ajaxRequest.responseText;
                        console.log(jsonResponse);
                        //alert("Jsonresponse:"+jsonResponse);
                        var responseObj = JSON.parse(jsonResponse);
                        var salida = "";
                        if (responseObj.hasOwnProperty("Error")) {
                            alert(responseObj.Error);
                            
                        } else {
                            if (responseObj.send === "Mensaje Enviado") {
                                goAjaxChatUpdate();
                            } 
                        }
                    }
                }
            };
            var msg=encodeURIComponent(document.getElementById("msg").value);
            ajaxRequest.open("POST", "/Amazon/ServletChat", true);
            ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajaxRequest.send("msg="+msg);
        }   
}
function goAjaxChatUpdate() {
        var ajaxRequest = CreateRequest();
        if (ajaxRequest !== false) {
            ajaxRequest.onreadystatechange = function () {
                if (ajaxRequest.readyState === 4) {
                    if (ajaxRequest.status === 200) {
                        var jsonResponse = ajaxRequest.responseText;
                        console.log(jsonResponse);
                        document.getElementById("sala").innerHTML = jsonResponse;
                    }
                }
            };
            ajaxRequest.open("POST", "/Amazon/ServletChatGetMessages", true);
            ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajaxRequest.send();
        }   
}
function goAjaxSendEmail() {
        var ajaxRequest = CreateRequest();
        if (ajaxRequest !== false) {
            ajaxRequest.onreadystatechange = function () {
                if (ajaxRequest.readyState === 4) {
                   var jsonResponse = ajaxRequest.responseText;
                        console.log(jsonResponse);
                        //alert("Jsonresponse:"+jsonResponse);
                        var responseObj = JSON.parse(jsonResponse);
                        var salida = "";
                        if (responseObj.hasOwnProperty("Error")) {
                            alert(responseObj.Error);
                            
                        } else {
                            if (responseObj.send === "Mensaje Enviado") {
                                alert("El mensaje fue enviado exitosamente");
                            } 
                        }
                }
            };
            var nombre = document.getElementById("Nombre_Contacto").value;
            var email = document.getElementById("email_Contacto").value;
            var mensaje = document.getElementById("mensaje").value;
            ajaxRequest.open("POST", "/Amazon/ContactEmail", true);
            ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajaxRequest.send("nombre="+nombre+"&email="+email+"&mensaje="+mensaje);
        }   
}