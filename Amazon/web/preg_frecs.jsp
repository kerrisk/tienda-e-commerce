<%-- 
    Document   : preg_frecs
    Created on : 17-dic-2018, 2:49:16
    Author     : Kristian
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Preguntas Frecuentes</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="js/ajaxCreateRequest.js" type="text/javascript"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
    </head>
    <body>
        <%@include file="header.jsp" %>
        <div class="container">
            <article>
                <header style="margin-top: 40px;">
                    <div class="left">
                        <i class="fas fa-angle-right"></i><span>Preguntas Frecuentes</span>
                    </div>
                    <div class="right">
                        NOSOTROS
                    </div>
                </header>
               <hr>
                <div>
                    <p class="main">¿Cuál es el costo de envío?</p>
                    <p class="sec">
                      <ul>
                        <li>El costo de envío será mostrado en base al total de la compra y ubicación, en el checkout, en el momento previo a la compra.</li>
                      </ul>
                    </p>
                    
                </div>
                
                <br>
                <div>
                    <p class="main">¿Dónde puedo recibir mi pedido?</p>
                    <p class="sec">
                    <ul>
                        <li>Realizamos envíos a todo el país.</li>
                    </ul>
                    </p>
                    
                </div>
                <br>
                <div>
                    <p class="main">¿Cuánto tarda en llegar el pedido?</p>
                    <p class="sec">
                        <ul>
                            <li>Los tiempos y costos de envio dependen de la conpañia de envios que se elija.</li>
                            <li>Su compra será embalada dentro de las 48hs-72 hs hábiles de recibido y confirmado el pago de la misma. </li>
                            <li>Una vez despachado el mismo recibirán de parte de Clottilda  un mail con el codigo de seguimiento</li>
                            <li>Nosotros no tenemos injerencias en los envíos de Compañias externas</li>
                        </ul>
                    </p>
                    
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div>
                    <p class="main">¿Cuales son los metodos de pago disponibles?</p>
                    <p class="sec">
                        <ul>
                            <li>Únicamente manejamos Tarjeta de crédito (MaterCard, American Express, VISA) y PAYPAL</li>
                        </ul>
                    </p>
                   
                </div>
                <br>
                <br>
                <div>
                    <p class="main">¿Qué pasa con la información que proporciono a la tienda?</p>
                    <p class="sec">
                        <ul>
                            <li>Su informacion tiene un cifrado seguro 100% certificacion ISO 17902, ni nosotros la podemos ver.</li>
                        </ul>
                    </p>
                   
                </div>
                <div>
                    <img src="images/AWS-Progression-Inforgraphic-01.png" alt="">
                </div>
            </article>
            <footer>
                <div class="left">
                    <a href="#"><i class="fas fa-rss-square"></i><span>Suscribete</span></a>
                    <a href="https://www.twitter.com"><i class="fab fa-twitter"></i><span>Siguenos en Twitter</span></a>
                    <a href="https://www.facebook.com"><i class="fab fa-facebook"></i></a>
                    <a href="https://www.google.com"><i class="fab fa-google-plus-g"></i></a>
                    <a href="https://www.blogger.com"><i class="fab fa-blogger"></i></a>

                </div>
                <div class="right">
                    <img src="images/1200px-Apple_logo_black.svg.png" alt="">
                    <img src="images/gentoo-g.png" alt="">
                    <img src="images/1280px-RedHat.svg.png" alt="">
                </div>
            </footer>
        </div>
        <jsp:include page="footer.html"/>
    </body>
</html>

