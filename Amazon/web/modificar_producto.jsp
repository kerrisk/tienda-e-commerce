<%-- 
    Document   : modificar_producto
    Created on : 18-dic-2018, 4:27:36
    Author     : Kristian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar Productos</title>
        <%@include file="head.html" %>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <div class="container">
            <article>
                <header style="margin-top: 40px;">
                    <div class="left">
                       <i class="fas fa-angle-right"></i><span style="font-size: 32px"> Modificar Productos</span>
                    </div>
                    <div class="right">
                         Administracion
                    </div>
                </header>
               <hr>
                
                <div>
                    <p class="sec">
                 
                  <%
             LinkedList <miClase.Producto> productos=miClase.Producto.getProductos(0);
                    Producto product_temporal;
                    Iterator <miClase.Producto>p = productos.iterator();
                while(p.hasNext()){
                   product_temporal=p.next();
                //System.out.println("1");
                %>
                <%
                byte[] imageData = product_temporal.getImagen(); 
                            String data = "data:image/jpeg;base64," + DatatypeConverter.printBase64Binary(imageData);

                %>
                <div class="containerCar">
                    <div class="polaroid">
                        <img src="<%=data%>"  class="imageCar" style="width:100%">
                        <div class="middle">
                            <button class="textBuy" onclick="editar_producto.jsp">MODIFICAR</button>
                            <button class="textBuy" onclick="goAjaxDeleteProduct(<%=product_temporal.getId()%>)">ELIMINAR</button>
                          </div>

                        <p><%=product_temporal.getDescripcion()%></p>
                        <br>
                        <p>$<%=product_temporal.getPrecio()%></p>
                    </div>
                 </div>
                 <hr class="line">
                    
                <%
                    
                 //System.out.println("2");
                }
                %>
                        
                    </p>
                   
                </div>
                <br>
                <br>
                
                
            </article>
            <footer>
                <div class="left">
                    <a href="#"><i class="fas fa-rss-square"></i><span>Suscribete</span></a>
                    <a href="https://www.twitter.com"><i class="fab fa-twitter"></i><span>Siguenos en Twitter</span></a>
                    <a href="https://www.facebook.com"><i class="fab fa-facebook"></i></a>
                    <a href="https://www.google.com"><i class="fab fa-google-plus-g"></i></a>
                    <a href="https://www.blogger.com"><i class="fab fa-blogger"></i></a>

                </div>
                <div class="right">
                    <img src="images/1200px-Apple_logo_black.svg.png" alt="">
                    <img src="images/gentoo-g.png" alt="">
                    <img src="images/1280px-RedHat.svg.png" alt="">
                </div>
            </footer>
        </div>
        <jsp:include page="footer.html"/>
    </body>
</html>
