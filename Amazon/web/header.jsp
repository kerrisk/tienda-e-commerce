<%@page import="javax.xml.bind.DatatypeConverter"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Iterator"%>
<%@page import="miClase.Producto"%>
<%@page import="java.util.ListIterator"%>
<%@page import="miClase.Usuario" %>
<%@page import="miClase.Carrito" %>

<%
    
    Usuario User = new Usuario("", "");
    Carrito carroTemp;
    HttpSession sesionOK = request.getSession();
    Boolean loggedin = sesionOK.getAttribute("User") != null;
    
    if (loggedin) {
        User = (Usuario) sesionOK.getAttribute("User");
    }
    if(sesionOK.getAttribute("carrito")==null){
         sesionOK.setAttribute("carrito", new Carrito());
         carroTemp= (Carrito)sesionOK.getAttribute("carrito");
         carroTemp.agregarArticulo(1, 2);
         carroTemp.agregarArticulo(2, 3);
         carroTemp.agregarArticulo(3, 1);
         sesionOK.setAttribute("carrito",carroTemp);
         out.println("<script>console.log(\"Se creo carrito en la sesion.\");</script>");
    }
    else {
        carroTemp= (Carrito)sesionOK.getAttribute("carrito");
    }
    
%>
<script>
    
   function goAjaxRegistro() {
        var p1 = document.getElementById("PasswordR");
        var p2 = document.getElementById("PasswordR2");
        if (document.getElementById("PasswordR.value") !== document.getElementById("PasswordR2.value")) {
            alert("Las contrase�as no coinciden");
            return false;
        }
        $('#registro').modal('hide');
        var ajaxRequest = CreateRequest();
        if (ajaxRequest !== false) {
            ajaxRequest.onreadystatechange = function () {
                if (ajaxRequest.readyState === 4) {
                    if (ajaxRequest.status === 200) {
                        var jsonResponse = ajaxRequest.responseText;
                        console.log(jsonResponse);
                        //alert("Jsonresponse:"+jsonResponse);
                        var responseObj = JSON.parse(jsonResponse);

                        var salida = "";
                        if (responseObj.hasOwnProperty("Error")) {
                            alert(responseObj.Error);
                        } else {
                            if (responseObj.loggedIn !== false) {
                                salida = "<div class=\"top\">\
                             " + responseObj.username + "\
                            </div>\
                            <div class=\"bottom\">\
                            <a href=\"cerrarsesion.jsp\">Log out</a>\
                            </div>";
                                console.log(salida);
                                console.log(document.getElementById("logg"));
                                document.getElementById("logg").innerHTML = salida;
                            } else {
                                alert("No se pudo registrar tu usuario, intenta de nuevo m�s tarde o contacta a soporte al cliente.");
                                //$('#registro').modal('show');
                            }
                        }
                    }
                }
            };
            var username = encodeURIComponent(document.getElementById("UserR").value);
            var password = encodeURIComponent(document.getElementById("PasswordR").value);
            var estado = encodeURIComponent(document.getElementById("Estado").value);
            var calle = encodeURIComponent(document.getElementById("Calle").value);
            var nombre = encodeURIComponent(document.getElementById("Nombre").value);
            var cp = encodeURIComponent(document.getElementById("CP").value);
            var email = encodeURIComponent(document.getElementById("email").value);
            var respPreg = encodeURIComponent(document.getElementById("respPreg").value);
            var idPreg = encodeURIComponent(document.getElementById("idPreg").options[document.getElementById("idPreg").selectedIndex].value);
            console.log(idPreg);
            console.log(username);
            console.log(password);
            console.log(estado);
            console.log(calle);
            console.log(nombre);
            console.log(cp);
            ajaxRequest.open("POST", "/Amazon/checkRegistro", true);
            ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajaxRequest.send("UserR=" + username + "&PasswordR=" + password + "&Estado=" + estado + "&Calle=" + calle + "&Nombre=" + nombre + "&CP=" + cp + "&email=" + email + "&respPreg=" + respPreg + "&idPreg=" + idPreg);
        }

    }
    function goAjaxLogin() {
        $('#login').modal('hide');
        var ajaxRequest = CreateRequest();
        if (ajaxRequest !== false) {
            ajaxRequest.onreadystatechange = function () {
                if (ajaxRequest.readyState === 4) {
                    if (ajaxRequest.status === 200) {
                        var jsonResponse = ajaxRequest.responseText;
                        console.log(jsonResponse);
                        //alert("Jsonresponse:"+jsonResponse);
                        var responseObj = JSON.parse(jsonResponse);
                        var salida = "";
                        if (responseObj.hasOwnProperty("Error")) {
                            alert(responseObj.Error);
                            $('#login').modal('show');
                        } else {
                            if (responseObj.loggedIn !== false) {
                                salida = "<div class=\"top\">\
                             " + responseObj.username + "\
                            </div>\
                            <div class=\"bottom\">\
                            <a href=\"cerrarsesion.jsp\">Log out</a>\
                            </div>";
                                //console.log(salida);
                                //console.log(document.getElementById("logg"));
                                document.getElementById("logg").innerHTML = salida;
                                salida = "<button type=\"button\" class=\"btn btn-login btn-sm\" onclick=\"openNav()\">\
            <i class=\"fas fa-shopping-cart\"></i>\
        </button>\
        <a href=\"index.jsp\"><img src=\"images/logo.svg\" class=\"logo\"></a>";
                                document.getElementById("CarLog").innerHTML = salida;
                            } else {
                                alert("Usuario o Contrase�a incorrectos.");
                                $('#login').modal('show');
                            }
                        }
                    }
                }
            };
            var username = encodeURIComponent(document.getElementById("User").value);
            var password = encodeURIComponent(document.getElementById("Password").value);
            ajaxRequest.open("POST", "/Amazon/checkLogin", true);
            ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            ajaxRequest.send("User=" + username + "&Password=" + password);
        }
    }
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
      }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
      }
</script>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<header>
   <% //if (loggedin){ %>
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <h2 style="color:white"><i class="fas fa-shopping-cart"></i> Carrito</h2>
        
        <table id="carrito" class="carrito">
        <%
            
            
            ListIterator<Producto> iterador=carroTemp.getArticulos().listIterator();
            Producto product_temp;
                
            for(int i=0; i<carroTemp.getArticulos().size(); i++){
               product_temp=(Producto)carroTemp.getArticulos().get(i);
               //System.out.println("1");
                %>
                <%
                byte[] imageData = product_temp.getImagen(); 
                            String data = "data:image/jpeg;base64," + DatatypeConverter.printBase64Binary(imageData);
                
                %>
                <tr style="background-color: white; color: black;">
                <td><%=product_temp.getDescripcion()%></td>
                <td><img class="imgCar"src="<%=data%>" border=0/></td>
                <td><button style="background-color:none; border: 0" onclick="goAjaxRemoveCarrito(<%=i%>)"><i class="fas fa-trash-alt"></i></button></td>
                </tr>
                <tr>
                <td>Precio: $<%=product_temp.getPrecio()%></td>
                <td>Cantidad: <%=product_temp.getCantidad()%></td>
                
                
                
                </tr>
                <%
                    
                 //System.out.println("2");
            }
            %>
            <tr style="background-color: white; color: black;">
                <td colspan="3">Subtotal: <%=carroTemp.getSubtotal()%></td>
                
            </tr>
            <%
            //System.out.println("3");
        %>
        </table>
        <a href="venta.jsp" style="margin: 0 auto"> Finalizar Compra</a>
     </div>
     <%// } %>
    <div class="left"  id="CarLog">
        <% if (loggedin){ %>
        <button type="button" class="btn btn-login btn-sm" onclick="openNav()">
            <i class="fas fa-shopping-cart">(<%=carroTemp.getNum()%>)</i>
        </button> 
        <% } %>
        <a href="index.jsp"><img src="images/logo.svg" alt="" class="logo"></a>
    </div>
    <div class="center">
        <div class="input-group" id="SearchBar">
        <input type="text" class="form-control" placeholder="Search" id="txtSearch"/>
        <div class="input-group-btn">
          <button class="btn btn-primary" style="background-color: #f8991d; border-color: #f8991d" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
        <nav>
            <ul>
                <li class="dropdown">
                    <a href="#" class="dropbtn">Administracion<i class="fas fa-angle-down"></i>
                   <div class="dropdown-content">
                        
                        <a href="registroProd.jsp">Registrar Producto</a>
                        <a href="modificar_producto.jsp">Modificar Producto</a>
                        
                        <a href="ver_graficas.jsp">Mostrar Estadisticas</a>  
                    </div>
                </li>
                
<!--                <li class="dropdown">
                    <a href="#" class="dropbtn">Certificaciones Developer<i class="fas fa-caret-down"></i></a>
                     <div class="dropdown-content">
                        <a href="Cert_java.jsp">Java</a>
                        <a href="Cert_web.jsp">Web Developer</a>
                    </div>
                </li>-->
                <li class="dropdown">
                    <a href="ver_tienda.jsp" class="dropbtn"> Tienda <i class="fas fa-shopping-bag"></i></a>
                   
                </li>
                <li class="dropdown">
                    <a href="#" class="dropbtn">Nosotros<i class="fas fa-angle-down"></i>
                   <div class="dropdown-content">
                        
                        <a href="misionvision.jsp">Mision & Vision</a>
                        <a href="preg_frecs.jsp">Preguntas Frecuentes</a>
                        <a href="acercade.jsp">Acerca de...</a>  
                    </div>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropbtn">Soporte<i class="fas fa-angle-down"></i>
                    <div class="dropdown-content">
                        
                        <a href="#">Contacto</a>
                        <a href="#">Atenci�n al cliente</a>
                    </div>
                </li>
            </ul>
        </nav>
    </div>
    
   
    
    <div class="right" id="logg">
        <%if (loggedin){ %>
        <div class="top">
            <%=User.getUsuario()%>
        </div>
        <div class="bottom">
            <a href="cerrarsesion.jsp">Log out</a>
        </div>
        <%} else { %>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-login btn-sm" data-toggle="modal" data-target="#login">
            Login
        </button>
        <!-- Modal -->
        <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="loginTitle">Login</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="">
                            <div class="form-group">
                                <label for="User">Usuario</label>
                                <input type="text" required class="form-control" id="User" aria-describedby="emailHelp" placeholder="Ingresa tu usuario" name="User">
                            </div>
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input type="password" required class="form-control" id="Password" placeholder="Password" name="Password">
                            </div>
                            <input type="button" value="submit" class="btn btn-primary" onclick="goAjaxLogin()"></input>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" type="button"  class="btn btn-login btn-sm" data-toggle="modal" data-target="#registro" >Registro</button>


                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>
        <!-- Modal de Registro-->
        <div class="modal fade" id="registro" tabindex="-1" role="dialog" aria-labelledby="registroTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="registroTitle">Registro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="User">Usuario</label>
                                <input type="text" required class="form-control" id="UserR" aria-describedby="emailHelp" placeholder="Ingresa tu usuario" name="UserR">
                            </div>
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input type="password" required class="form-control" id="PasswordR" placeholder="Password" name="PasswordR">
                            </div>
                            <div class="form-group">
                                <label for="Password">De nuevo Password</label>
                                <input type="password" required class="form-control" id="PasswordR2" placeholder="Reingresa tu Password" name="PasswordR2">
                            </div>
                            <div class="form-group">
                                <label for="Nombre">Nombre</label>
                                <input type="text" required class="form-control" id="Nombre" placeholder="ingresa tu nombre" name="Nombre">
                            </div>
                            <div class="form-group">
                                <label for="Nombre">Estado</label>
                                <input type="text" required class="form-control" id="Estado" placeholder="Aguascalientes" name="Estado">
                            </div>
                            <div class="form-group">
                                <label for="Calle">Calle</label>
                                <input type="text" required class="form-control" id="Calle" placeholder="ingresa tu direcci�n" name="Calle">
                            </div>
                            <div class="form-group">
                                <label for="CP">C�digo Postal</label>
                                <input type="number" required class="form-control" id="CP" placeholder="ingresa tu nombre" name="CP">
                            </div>
                            <div class="form-group">
                                <label for="email">Correo Electronico</label>
                                <input type="email" required class="form-control" id="email" placeholder="Ingresa tu Correo" name="email">
                            </div>
                            <div class="form-group">
                                <label for="idPreg">Pregunta de Seguridad</label>
                                <select id="idPreg" name="idPreg" class="form-control">
                            <%
                                LinkedList <miClase.PreguntasSeguridad> depas = miClase.PreguntasSeguridad.getPreguntas();
                                Iterator <miClase.PreguntasSeguridad>i = depas.iterator();
                                miClase.PreguntasSeguridad temp;
                                while (i.hasNext()){
                                    temp = i.next();
                                        %>
                                        <option value="<%=temp.getId()%>"><%=temp.getRespuesta()%></option>
                                        <%
                                }
                            %>
                            
                                </select>
                            <input type="text" max="100" required class="form-control" id="respPreg" placeholder="Respuesta" name="respPreg">
                            </div>
                            
                            <input type="button" value="submit" class="btn btn-primary" onclick="goAjaxRegistro()"></input>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <% }%>
    </div>
</header>
