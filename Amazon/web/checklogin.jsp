<%-- 
    Document   : checklogin
    Created on : 28/10/2018, 11:07:29 AM
    Author     : erikgoh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.io.File" %>
<%@page import="java.io.FileNotFoundException" %>
<%@page import="java.util.Scanner" %>
<%@page session="true" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String User = "";
            String Password = "";
            String UserS;
            String PasswordS;
            Boolean band = false;
            if (request.getParameter("User") != null) {
                User = request.getParameter("User");
            }
            if (request.getParameter("Password") != null) {
                Password = request.getParameter("Password");
            }
            out.println(User);
            out.println(Password);
            String Archivo = application.getRealPath("/") + "Cuentas.txt";
            File file = new File(Archivo);
            Scanner sc = new Scanner(file);

            // we just need to use \\Z as delimiter 
            sc.useDelimiter(":");
            while (sc.hasNext()) {
                UserS = sc.next();
                PasswordS = sc.next();
                out.println("--------------");
                out.println(UserS);
                out.println(PasswordS);
                out.println("--------------");
                out.println("<br>");
                if (User.equals(UserS) && Password.equals(PasswordS)) {
                    band = true;
                    HttpSession sesionOk = request.getSession();
                    sesionOk.setAttribute("User", User);
        %>
        <jsp:forward page="index.jsp">
            <jsp:param name="saludo" value="Buen Día" />
        </jsp:forward>
        <%
                }
            }
            if (!band) {
        %>
        <jsp:forward page="index.jsp">
            <jsp:param name="error" value="Usuario y/o Contraseña incorrecto."/>
        </jsp:forward>
        <%
            }

        %>
        <h1>Hello World!</h1>
    </body>
</html>
