<%-- 
    Document   : ver_graficas
    Created on : 15-dic-2018, 19:44:46
    Author     : Kristian
--%>

<%@page session="true"%>
<!DOCTYPE html>
<html>
    <head>
         <head>
        <meta charset="UTF-8">
        <title>Tienda de Electronicos</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="js/ajaxCreateRequest.js" type="text/javascript"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
    </head>
    <body>
        <%--<jsp:include page="header.jsp"/>--%>
        <%@include file="header.jsp" %>  
        <div class="container">

            <article>
                <header>
                    <div class="left">
                        <i class="fas fa-angle-right"></i><span>Estadisticas</span>
                    </div>
                    <div class="right">
                         Administracion
                    </div>
                </header>
                <hr>
                <div>
                    <p class="main">Gafica de INVENTARIO por producto</p>
                    <p class="sec">Se muestra una grafica donde se visualiza cantidad de ventas por producto</p>
                    
                    <a href="barra1">Ver Grafica</a>
                    <hr class="linea">
                </div>
                <div>
                    <p class="main">Grafica de ventas por Cliente</p>
                    <p class="sec">Se muestra una grafica donde se visualiza cantidad de ventas por producto</p>
                    
                    <a href="barra2.jsp">Ver Grafica</a>
                    <hr class="linea">
                </div>
                <div>
                    <img src="images/AWS-Progression-Inforgraphic-01.png" alt="">
                </div>
            </article>
            <footer>
                <div class="left">
                    <a href="#"><i class="fas fa-rss-square"></i><span>Suscribete</span></a>
                    <a href="https://www.twitter.com"><i class="fab fa-twitter"></i><span>Siguenos en Twitter</span></a>
                    <a href="https://www.facebook.com"><i class="fab fa-facebook"></i></a>
                    <a href="https://www.google.com"><i class="fab fa-google-plus-g"></i></a>
                    <a href="https://www.blogger.com"><i class="fab fa-blogger"></i></a>

                </div>
                <div class="right">
                    <img src="images/1200px-Apple_logo_black.svg.png" alt="">
                    <img src="images/gentoo-g.png" alt="">
                    <img src="images/1280px-RedHat.svg.png" alt="">
                </div>
            </footer>	
        </div>
        <jsp:include page="footer.html"/>
    </body>
</html>
