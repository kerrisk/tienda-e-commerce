<%@page import="miClase.Descuentos"%>
<!--/* 
 * Copyright (C) 2018 erikgoh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */-->
<%@page session="true"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Tienda de Electronicos</title>
        <%@include file="head.html" %>
        <script>
            function myFunction() {
              var copyText = document.getElementById("myInput");
              copyText.select();
              document.execCommand("copy");

              var tooltip = document.getElementById("myTooltip");
              tooltip.innerHTML = "Copied: " + copyText.value;
            }

            function outFunc() {
              var tooltip = document.getElementById("myTooltip");
              tooltip.innerHTML = "Copy to clipboard";
            }
        </script>
    </head>
    <body class="">
        <%--<jsp:include page="header.jsp"/>--%>
        <%@include file="header.jsp" %>
        
        <!--        Carrusel Bootsrap -->
        <div id="carousel" class="carousel slide" data-ride="carousel" >
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="images/GW_DesktopHero_primerefreshH4_1500X300._CB482732913_.jpg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/551218_gw_announce_copy_desktop_1500x300._CB492370810_.jpg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/desktopHero_librosNovDic_1500x300._CB482154680_.jpg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/HD_DesktopHero_AmazonFashion_1500X300._CB478971957_.jpg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/MX_Gateway_Hero_Branding_Desktop_AMU_1500x300_1x._CB479441256_.jpg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/Amazon_GW_DesktopHero_PVD583_Comedy_1500x300_es_MX._CB478811553_.jpg">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="images/XCM_Manual_1123307_GW_Images_blenders_1500x300_Kitchen_1123307_mx_kitchen_a_gw_desktop_hero_1500x300_1530895077_jpg._CB473030625_.jpg">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!--        Fin Carrusel Bootsrap -->
        
        
        <div class="container">

            <article>
                <header>
                    <div class="left">
                        <i class="fas fa-angle-right"></i><span style="font-size: 32px"> Ofertas</span>
                    </div>
                    <div class="right">
                        Inicio
                    </div>
                </header>
                <hr>
                <div>
                    <p class="main">CUPON- Envio Gratis</p>
                    <p class="sec">
                        <img src="images/enviogratis.png">
                    </p>
                 

                         <p class="main" style="text-align:center; color: #cccc00">CUPON: HelloAmazon</p>
                    </p>
                    
                    <hr class="linea">
                </div>
                <div>
                    <p class="main">CUPON- Iphone XS MAX 15% DESC</p>
                    <img src="images/iphoneXS.png">
                      <p class="main" style="text-align:center; color: #cccc00">CUPON: Helloiphone</p>

                </p>
                    <hr class="linea">
                </div>
                <div>
                    <p class="main">CUPON- Aprovecha los descuentos en el departamento de Televisiones!</p>
                    <img src="images/tvs.png">
                         <p class="main" style="text-align:center; color: #cccc00">CUPON: HelloTV</p>
                        
                    <hr class="linea">
                </div>
                
                <div>
                    <p class="main">CUPON- CUPON ALEATORIO!</p>
                    <img src="images/Cupones.png">
                     <%         int i=0;
                                LinkedList <miClase.Descuentos> desc = miClase.Descuentos.getDescuentos();
                               i = (int) (Math.random() * desc.size()) + 1;
                                Descuentos dTemp = desc.get(i);
                            %>
                    <p class="main" style="text-align:center; color: #cccc00">CUPON: <%=dTemp.getCodigo()%></p>
                    <hr class="linea">
                </div>
               
            </article>
            <br>
            <br>
            <br>
            <footer>
                <div class="left">
                    <a href="#"><i class="fas fa-rss-square"></i><span>Suscribete</span></a>
                    <a href="https://www.twitter.com"><i class="fab fa-twitter"></i><span>Siguenos en Twitter</span></a>
                    <a href="https://www.facebook.com"><i class="fab fa-facebook"></i></a>
                    <a href="https://www.google.com"><i class="fab fa-google-plus-g"></i></a>
                    <a href="https://www.blogger.com"><i class="fab fa-blogger"></i></a>

                </div>
                <div class="right">
                    <img src="images/1200px-Apple_logo_black.svg.png" alt="">
                    <img src="images/gentoo-g.png" alt="">
                    <img src="images/1280px-RedHat.svg.png" alt="">
                </div>
            </footer>	
        </div>
        <jsp:include page="footer.html"/>
    </body>
</html>