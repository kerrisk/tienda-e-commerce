<%-- 
    Document   : registroProd.jsp
    Created on : 3/12/2018, 01:28:42 AM
    Author     : erikgoh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.LinkedList,java.util.Iterator" %>
<%--<jsp:useBean id="depa" class="miClase.Departamento"/>--%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro de Productos</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="js/ajaxCreateRequest.js" type="text/javascript"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
    </head>
    <body>
        <%@include file="header.jsp" %>

        <h1>Formulario de Registro de Productos</h1>
        <form class="form-horizontal" action="RegistrarProd" method="POST" enctype="multipart/form-data">
            <fieldset>

                <!-- Form Name -->
                <legend>Form Name</legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Name">Nombre</label>  
                    <div class="col-md-4">
                        <input id="Name" name="Name" type="text" placeholder="" class="form-control input-md">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Precio">Precio</label>  
                    <div class="col-md-4">
                        <input id="Precio" name="Precio" type="number" placeholder="" class="form-control input-md">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Existencia">Existencia</label>  
                    <div class="col-md-4">
                        <input id="Existencia" name="Existencia" type="number" placeholder="" class="form-control input-md">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="imagen">Imagen</label>  
                    <div class="col-md-4">
                         <input type="file" name="imagen" id="imagen">

                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Id_Departamento">Departamento</label>
                    <div class="col-md-4">
                        <select id="Departamento" name="Id_Departamento" class="form-control">
                            <% 
                                LinkedList <miClase.Departamento> depas = miClase.Departamento.getDepartamentos();
                                Iterator <miClase.Departamento>i = depas.iterator();
                                miClase.Departamento temp;
                                while (i.hasNext()){
                                    temp = i.next();
                                        %>
                                        <option value="<%=temp.getId_Departamento()%>"><%=temp.getDescripcion()%></option>
                                        <%
                                }
                            %>
                            
                        </select>
                    </div>
                </div>

               
                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <input id="submit" type="submit" name="submit" class="btn btn-primary"></input>
                    </div>
                </div>
            </fieldset>
        </form>
        <footer>
            <div class="left">
                <span>2018 <i class="far fa-registered"></i>Amazon. Todos los derechos reservados. 3-297-15728-0.</span>
            </div>
            <div class="right green">
                @Amazon.com.mx
            </div>
        </footer>
    </body>
</html>
