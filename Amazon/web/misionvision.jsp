<%-- 
    Document   : misionvision
    Created on : 17-dic-2018, 2:19:51
    Author     : Kristian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mision y Vision</title>
        <link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device=width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"  crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="js/ajaxCreateRequest.js" type="text/javascript"></script>
        <link id="favicon" rel="icon" type="image/png" href="images/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">
    </head>
    <body>
        <%@include file="header.jsp" %>
        <div class="container">
            <article>
                <header style="margin-top: 40px;">
                    <div class="left">
                        <i class="fas fa-angle-right"></i><span>Misión & Visión</span>
                    </div>
                    <div class="right">
                        NOSOTROS
                    </div>
                </header>
                <hr>
                <div>
                    <p class="main">Nuestra Misión</p>
                    <p class="sec">
                        Nuestra misión es “aprovechar la tecnología y la experiencia de nuestros empleados para ofrecer a los consumidores la mejor experiencia de compra en internet”. La visión de Amazon es “ser la empresa más centrada en el cliente del mundo, donde los clientes pueden descubrir cualquier cosa que puedan querer comprar online, y comprometerse a ofrecer los precios más bajos posibles”. En cuanto a sus valores: obsesión por el consumidor, innovación, predisposición a la acción, apropiación del negocio, excelencia en la contratación de empleados, austeridad.
                    </p>
                    <p class="sec">
                        En la actualidad, Amazon está totalmente diversificada en diferentes líneas de productos y servicios, ofreciendo vídeos, música, apps, servicios en la nube (cloud computing), libros, electrónica y ordenadores, bricolaje y menaje del hogar, productos de belleza y cuidado personal, alimentación, vino, juguetes y productos infantiles, ropa, calzado y joyería, deportes y actividades al aire libre, automoción y productos industriales, servicios de pago, entre otros servicios. Todo ello con marcas líderes del mercado así como con productos propios (AmazonBasics, Amazon Publishing, Pinzon, Kindle, Fire Phone, Amazon Fire TV, Servicios Web de Amazon, Amazon Local, Amazon Wireless, Local Register o incluso la moneda virtual Amazon coin, entre otros).
                    </p>
                    <hr class="linea">
                </div>
                <div>
                    <p class="main">Visión</p>
                    <p class="sec">
                        La innovación tecnológica lidera el crecimiento de Amazon para ofrecer a los consumidores más tipos de productos, de forma más eficiente y a precios menores. Desde 1995, Amazon ha aumentado considerablemente su cartera de productos, sus webs de eCommerce (comercio online) internacionales, y su red de centros logísticos y de atención al cliente.
                    </p>
                    <p class="sec">
                        En 2000, Amazon empezó a ofrecer sus plataformas de eCommerce a otras cadenas de distribución y pequeños minoristas. Hoy, cientos de miles de marcas (se calcula que algo menos de un millón de socios) aumentan sus ventas y llegan a nuevos clientes gracias al efecto palanca de las plataformas de eCommerce de Amazon. Las ventas a través de Amazon Partners o “Third-Party Sellers” llegan a representar en torno al 40% de las ventas de Amazon.
                    </p>
                    <p class="sec">
                        Los servicios web de Amazon (AWS en sus siglas en inglés) ofrecen servicios e infraestructuras “en la nube” (cloud computing) para que los desarrolladores puedan usarlos en cualquier tipo de negocio. Amazon es uno de los mayores proveedores mundiales de servicios integrales de cloud computing de amplia capacidad.
                    </p>
                    <hr class="linea">
                </div>

                <div>
                    <img src="images/AWS-Progression-Inforgraphic-01.png" alt="">
                </div>
            </article>
            <footer>
                <div class="left">
                    <a href="#"><i class="fas fa-rss-square"></i><span>Suscribete</span></a>
                    <a href="https://www.twitter.com"><i class="fab fa-twitter"></i><span>Siguenos en Twitter</span></a>
                    <a href="https://www.facebook.com"><i class="fab fa-facebook"></i></a>
                    <a href="https://www.google.com"><i class="fab fa-google-plus-g"></i></a>
                    <a href="https://www.blogger.com"><i class="fab fa-blogger"></i></a>

                </div>
                <div class="right">
                    <img src="images/1200px-Apple_logo_black.svg.png" alt="">
                    <img src="images/gentoo-g.png" alt="">
                    <img src="images/1280px-RedHat.svg.png" alt="">
                </div>
            </footer>
        </div>
        <jsp:include page="footer.html"/>
    </body>
</html>
