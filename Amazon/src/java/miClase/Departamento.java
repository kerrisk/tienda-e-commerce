/*
 * Copyright (C) 2018 erikgoh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package miClase;

/**
 *
 * @author erikgoh
 */
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
public class Departamento {
    private String Descripcion;
    private int Id_Departamento;

    public Departamento(String Descripcion, int Id_Departamento) {
        this.Descripcion = Descripcion;
        this.Id_Departamento = Id_Departamento;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public int getId_Departamento() {
        return Id_Departamento;
    }

    public void setId_Departamento(int Id_Departamento) {
        this.Id_Departamento = Id_Departamento;
    }
    
    public static LinkedList getDepartamentos(){
    MySqlConn objConn = new MySqlConn();
        LinkedList <Departamento> depas = new LinkedList();
        try {
            objConn.PrepareStmt("SELECT * FROM Departamento"); //Se utiliza un PreparedStatement para evitar ataques por inyeccion SQL. mas info https://javabypatel.blogspot.com/2015/09/how-prepared-statement-in-java-prevents-sql-injection.html
            //System.out.println("Selected");
            //System.out.println("Replaced");
            if (objConn.pstmt.execute()) {
                
                objConn.rs = objConn.pstmt.getResultSet();
                //objConn.rs.first();
                while(objConn.rs.next()){
                    depas.add(new Departamento(objConn.rs.getString(2),objConn.rs.getInt(1)));
                    
                }
                //for (int i = 0; i< depas.size(); i++){
                //    System.out.println(depas.get(i).toString());
                //}
                objConn.closeRsStmt();
                objConn.closeConnection();
                return depas;
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeRsStmt();
        objConn.closeConnection();
        return null;
}

    @Override
    public String toString() {
        return Descripcion+" "+Id_Departamento;
    }
    
}
