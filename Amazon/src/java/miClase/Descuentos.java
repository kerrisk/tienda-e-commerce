/*
 * Copyright (C) 2018 Kristian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package miClase;

import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Kristian
 */
public class Descuentos {
    private int id_Descuento;
    private int tipo;
    private String codigo;
    private int porcentaje;
    private int idProd;
    private int idDep;

    public Descuentos(int id_Descuento, int tipo, String codigo, int porcentaje) {
        this.id_Descuento = id_Descuento;
        this.tipo = tipo;
        this.codigo = codigo;
        this.porcentaje = porcentaje;
    }
    

    public int getId_Descuento() {
        return id_Descuento;
    }

    public int getIdProd() {
        return idProd;
    }

    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }

    public int getIdDep() {
        return idDep;
    }

    public void setIdDep(int idDep) {
        this.idDep = idDep;
    }

    public void setId_Descuento(int id_Descuento) {
        this.id_Descuento = id_Descuento;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }
    public static LinkedList getDescuentos(){
    MySqlConn objConn = new MySqlConn();
        LinkedList <Descuentos> descuentos = new LinkedList();
        
        try {
            
                objConn.PrepareStmt("SELECT * FROM Descuento"); //Se utiliza un PreparedStatement para evitar ataques por inyeccion SQL. mas info https://javabypatel.blogspot.com/2015/09/how-prepared-statement-in-java-prevents-sql-injection.html
            
            
            
            //System.out.println("Selected");
            //System.out.println("Replaced");
            if (objConn.pstmt.execute()) {
                
                objConn.rs = objConn.pstmt.getResultSet();
                //objConn.rs.first();
                while(objConn.rs.next()){
                    descuentos.add(new Descuentos(objConn.rs.getInt(1),objConn.rs.getInt(2),objConn.rs.getString(3),objConn.rs.getInt(4)));
                    
                }
                //for (int i = 0; i< depas.size(); i++){
                //    System.out.println(depas.get(i).toString());
                //}
                objConn.closeRsStmt();
                objConn.closeConnection();
                return descuentos;
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeRsStmt();
        objConn.closeConnection();
        return null;
    }
    
        public static boolean verificarDescuento(String codigo){
    MySqlConn objConn = new MySqlConn();
        
        try {
            
                objConn.PrepareStmt("SELECT * FROM Descuento WHERE Codigo=?"); //Se utiliza un PreparedStatement para evitar ataques por inyeccion SQL. mas info https://javabypatel.blogspot.com/2015/09/how-prepared-statement-in-java-prevents-sql-injection.html
            
            
            
            objConn.pstmt.setString(1,codigo);
            
            
            //System.out.println("Selected");
            //System.out.println("Replaced");
            if (objConn.pstmt.execute()) {
                
            }
            return true;
            
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeRsStmt();
        objConn.closeConnection();
        return false;
    }
    
     
}
