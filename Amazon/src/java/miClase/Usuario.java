/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miClase;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

/**
 *
 * @author erikgoh
 */
public class Usuario {

    private String Usuario;
    private String Password;
    private String Estado;
    private String Calle;
    private String Nombre;
    private String CP;
    private String Tipo;
    private String Email;
    private String Respuesta_Pregunta_Recuperacion;
    private int Id_Pregunta_Recuperacion;
    private int Bloqueado = 0;
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static String SALT = "FQedRU7e2HeQtM8hfKZvtcqKTUjApHbCpt4Q4Yo3QJi";
    private MySqlConn objConn = null;

    public Usuario(String Usuario, String Password, String Estado, String Calle, String Nombre, String CP, String Correo, String Pregunta_Recuperacion, int Id_Pregunta_Recuperacion) {
        this.Usuario = Usuario;
        this.Password = Password;
        this.Estado = Estado;
        this.Calle = Calle;
        this.Nombre = Nombre;
        this.CP = CP;
        this.Email = Correo;
        this.Respuesta_Pregunta_Recuperacion = Pregunta_Recuperacion;
        this.Id_Pregunta_Recuperacion = Id_Pregunta_Recuperacion;
        this.Tipo = "usuario";
    }

    public Usuario(String Usuario, String Password) {
        this.Usuario = Usuario;
        this.Password = Password;
        this.Tipo = "usuario";
    }

    public int getBloqueado() {
        return Bloqueado;
    }

    public void setBloqueado(int Bloqueado) {
        this.Bloqueado = Bloqueado;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getRespuesta_Pregunta_Recuperacion() {
        return Respuesta_Pregunta_Recuperacion;
    }

    public void setRespuesta_Pregunta_Recuperacion(String Respuesta_Pregunta_Recuperacion) {
        this.Respuesta_Pregunta_Recuperacion = Respuesta_Pregunta_Recuperacion;
    }

    public int getId_Pregunta_Recuperacion() {
        return Id_Pregunta_Recuperacion;
    }

    public void setId_Pregunta_Recuperacion(int Id_Pregunta_Recuperacion) {
        this.Id_Pregunta_Recuperacion = Id_Pregunta_Recuperacion;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getCalle() {
        return Calle;
    }

    public void setCalle(String Calle) {
        this.Calle = Calle;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getCP() {
        return CP;
    }

    public void setCP(String CP) {
        this.CP = CP;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    /*-------------------------Encriptacion Contraseñas--------------------------*/
    // We need a bytesToHex method first. So, from -
    // http://stackoverflow.com/a/9855338/2970947
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    // Change this to something else.
    // A password hashing method.
    public static String hashPassword(String in) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(SALT.getBytes());        // <-- Prepend SALT.
            md.update(in.getBytes());
            // md.update(SALT.getBytes());     // <-- Or, append SALT.

            byte[] out = md.digest();
            return bytesToHex(out);            // <-- Return the Hex Hash.
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /*-------------------------Fin Encriptacion Contraseñas--------------------------*/
    public int checkPassword() {
        objConn = new MySqlConn();
        try {
            objConn.PrepareStmt("SELECT * FROM Usuario WHERE Usuario=?"); //Se utiliza un PreparedStatement para evitar ataques por inyeccion SQL. mas info https://javabypatel.blogspot.com/2015/09/how-prepared-statement-in-java-prevents-sql-injection.html
           //System.out.println("Selected");
            objConn.pstmt.setString(1, this.Usuario);
            //System.out.println("Replaced");
            if (objConn.pstmt.execute()) {
               // System.out.println("No error");
                objConn.rs = objConn.pstmt.getResultSet();
                if (!objConn.rs.isBeforeFirst()) {
                    System.out.println("No hay registro de ese usuario");
                    return 0;
                }
                objConn.rs.next();
                System.out.println(objConn.rs);
                //System.out.println("No errorx2");
                if (this.Password.compareTo(objConn.rs.getString("Password")) == 0) {
                    //System.out.println("No errorx3");

                    objConn.closeRsStmt();
                    objConn.closeConnection();
                    return obtenerUsuario();
                }
                return -4;
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeConnection();
        return -1;
    }

    public int obtenerUsuario() {
        objConn = new MySqlConn();
        try {
            objConn.PrepareStmt("SELECT * FROM Usuario Where Usuario = ? AND Password = ?"); //Se utiliza un PreparedStatement para evitar ataques por inyeccion SQL. mas info https://javabypatel.blogspot.com/2015/09/how-prepared-statement-in-java-prevents-sql-injection.html
            //System.out.println("Selected");
            objConn.pstmt.setString(1, Usuario);
            objConn.pstmt.setString(2, Password);
            //System.out.println("Replaced");
            if (objConn.Ejecutar()) {
               // System.out.println("No error");
                if (objConn.rs.next()) {
                    this.setNombre(objConn.rs.getString(2));
                    this.setCP(objConn.rs.getString(3));
                    this.setCalle(objConn.rs.getString(4));
                    this.setEstado(objConn.rs.getString(5));
                    this.setTipo(objConn.rs.getString(8));
                    this.setEmail(objConn.rs.getString(9));
                    this.setRespuesta_Pregunta_Recuperacion(objConn.rs.getString(10));
                    this.setId_Pregunta_Recuperacion(objConn.rs.getInt(11));
                    this.setBloqueado(objConn.rs.getInt(12));
                    System.out.println(this.toString());
                    if (Bloqueado != 0) {
                        return -2;
                    }
                    return 1;
                } else {
                    
                    return -1;
                }
            }
            objConn.closeRsStmt();
            objConn.closeConnection();
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeRsStmt();
        objConn.closeConnection();
        return -1;
    }

    public boolean registrarUsuario() {
        objConn = new MySqlConn();
        try {
            objConn.PrepareStmt("INSERT INTO Usuario values (?,?,?,?,?,?,?,?,?,?,?,DEFAULT)"); //Se utiliza un PreparedStatement para evitar ataques por inyeccion SQL. mas info https://javabypatel.blogspot.com/2015/09/how-prepared-statement-in-java-prevents-sql-injection.html
            System.out.println("Selected");
            objConn.pstmt.setNull(1, 0);
            objConn.pstmt.setString(2, this.Nombre);
            objConn.pstmt.setInt(3, Integer.parseInt(this.CP));
            objConn.pstmt.setString(4, this.Calle);
            objConn.pstmt.setString(5, this.Estado);
            objConn.pstmt.setString(6, this.Usuario);
            objConn.pstmt.setString(7, this.Password);
            objConn.pstmt.setString(8, "usuario");
            objConn.pstmt.setString(9, this.Email);
            objConn.pstmt.setString(10, this.Respuesta_Pregunta_Recuperacion);
            objConn.pstmt.setInt(11, this.Id_Pregunta_Recuperacion);
            System.out.println("Replaced");
            objConn.pstmt.execute();
            System.out.println("No error");
            objConn.closeRsStmt();
            objConn.closeConnection();
            return true;

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeRsStmt();
        objConn.closeConnection();
        return false;
    }

    public static void main(String[] args) {
        System.out.println(hashPassword("Hello"));
        System.out.println(hashPassword("Hello"));
        System.out.println(hashPassword("Hello1"));
        System.out.println(hashPassword("Hello2"));
    }

    @Override
    public String toString() {
        return "Usuario{" + "Usuario=" + Usuario + ", Password=" + Password + ", Estado=" + Estado + ", Calle=" + Calle + ", Nombre=" + Nombre + ", CP=" + CP + ", Tipo=" + Tipo + ", Correo=" + Email + ", Pregunta_Recuperacion=" + Respuesta_Pregunta_Recuperacion + ", Id_Pregunta_Recuperacion=" + Id_Pregunta_Recuperacion + ", Bloqueado=" + Bloqueado + ", objConn=" + objConn + '}';
    }

}
