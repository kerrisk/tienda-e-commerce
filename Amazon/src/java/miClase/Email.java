/*
 * Copyright (C) 2018 Erik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package miClase;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

/**
 *
 * @author Erik
 */
public class Email {

    private HtmlEmail email;

    public Email() {
        email = new HtmlEmail(); //Se crea un objeto de tipo HtmlMail
        email.setDebug(true);
        email.setHostName("smtp.office365.com");//Sc coloca el servidor SMTP (Simple Mail Transfer Protocol)
        email.setSmtpPort(587); //Se configura el puerto
        email.setAuthenticator(new DefaultAuthenticator("al175415@edu.uaa.mx", "E2R8I0K9p9o8"));    //Se coloca el usuario y contraseña
        email.setStartTLSEnabled(true);
    }

    public boolean usuarioToSite(String Nombre, String from, String Mensaje) {
        try {
            email.setFrom("al175415@edu.uaa.mx");
            email.setSubject("El usuario "+Nombre +" con el correo: "+ from+" Tiene una duda"); //El asunto
            email.setHtmlMsg(Mensaje);
            email.addTo("al175415@edu.uaa.mx");
            email.send();
            return true;
        } catch (EmailException ex) {
            System.out.println(ex.toString());
        }
        return false;
    }
    public boolean usuarioNotaVenta(String Nombre, String from, String Mensaje) {
        try {
            System.out.println("No error");
            email.setFrom("al175415@edu.uaa.mx");
            email.setSubject("Nota de Venta Amazon"); //El asunto
            email.setHtmlMsg(Mensaje);
            email.addTo(from);
            email.send();
            return true;
        } catch (EmailException ex) {
            System.out.println(ex.toString());
        }
        return false;
    }
    public HtmlEmail getEmail() {
        return email;
    }

    public void setEmail(HtmlEmail email) {
        this.email = email;
    }

}
