/*
 * Copyright (C) 2018 Kristian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package miClase;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.ListIterator;
/**
 *
 * @author Kristian
 */
public class Carrito {
    private LinkedList <Producto>articulos;
    private float subtotal=0;
    private float gastosEnvio=150;
    private float total=0;
    
    public Carrito(){
        articulos=new LinkedList();
    }
    public LinkedList getArticulos() {
        return articulos;
    }

    public float getSubtotal() {
         ListIterator<Producto> iterador=articulos.listIterator();
            Producto product_temp;
            subtotal=0;
            while(iterador.hasNext()){
               product_temp=iterador.next();
               subtotal+=product_temp.getPrecio()*product_temp.getCantidad();
            }
                
                
        return subtotal;
    }

    public float getGastosEnvio() {
        return gastosEnvio;
    }
    
    public int getNum(){
        return articulos.size();
    }
    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
    }
    
    public void setArticulos(LinkedList articulos) {
        this.articulos = articulos;
    }
    
    public boolean agregarArticulo(int id, int cantidad){
        Producto product_temp=Producto.obtenerProducto(id, cantidad);
        if(product_temp==null){
            return false;
        }
        if(product_temp.getExistencia()-cantidad>=0){
            return articulos.add(Producto.obtenerProducto(id, cantidad));
        }
        else{
            
            return false;
        }
        
    }
    public boolean verificarYAplicarCupon(String cup){
        MySqlConn objConn = new MySqlConn();
        float descuento=0;
        int tipo=0;
        try {
            objConn.PrepareStmt("SELECT * FROM Descuento WHERE Codigo=?"); //Se utiliza un PreparedStatement para evitar ataques por inyeccion SQL. mas info https://javabypatel.blogspot.com/2015/09/how-prepared-statement-in-java-prevents-sql-injection.html
            objConn.pstmt.setString(1, cup);
            if (objConn.pstmt.execute()) {
                
                objConn.rs = objConn.pstmt.getResultSet();
                if (objConn.Ejecutar()){            
                if (objConn.rs.next()){
                    tipo=objConn.rs.getInt(2);
                    descuento=objConn.rs.getFloat(4);
                }
                switch(tipo){
                    case 1:    //Descuento en Producto
                        int id_prod=1;
                        descProd(id_prod, descuento);
                        break;
                    case 2:
                        
                        break;
                    case 3:
                        gastosEnvio=0;
                        break;
                        
                }
                
            }
                objConn.closeRsStmt();
                objConn.closeConnection();
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeRsStmt();
        objConn.closeConnection();
        
        return false;
    }
    public void descProd(int id_prod, float descuento){
        ListIterator<Producto> iterador=articulos.listIterator();
        Producto product_temp;
           while(iterador.hasNext()){
                product_temp=iterador.next();
               if(product_temp.getId()==id_prod){
                   product_temp.setPrecio((int)(product_temp.getPrecio()*(1-(descuento/100))));
                   
               }
               
            }  
    }
    
    public boolean eliminarArticulo(int i){   
        if(articulos.size()==0){
            return false;
        }
        if(articulos.remove(i)!=null){
            return true;
        }else{
            return false;
        }
                 
        
    }
    public void vaciarCarrito(int id){
        articulos.clear();
        
    }    
}
