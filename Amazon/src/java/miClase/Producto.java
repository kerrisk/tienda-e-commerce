/*
 * Copyright (C) 2018 erikgoh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package miClase;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.LinkedList;
import javax.servlet.http.Part;
/**
 *
 * @author erikgoh
 */
public class Producto {
   private int id;
   private int Precio;
   private int Existencia;
   private String Descripcion;
   private int Id_Departamento;
   private Part part;
   private int cantidad;
   private byte imagen[];

    public Producto(int Precio, int Existencia, String Descripcion, int Id_Departamento, Part image) {
        this.Precio = Precio;
        this.Existencia = Existencia;
        this.Descripcion = Descripcion;
        this.Id_Departamento = Id_Departamento;
        this.part = image;
    }

    public Producto(int id,int Precio, int Existencia, String Descripcion, int Id_Departamento, byte image[], int cantidad) {
        this.id= id;
        this.Precio = Precio;
        this.Existencia = Existencia;
        this.Descripcion = Descripcion;
        this.Id_Departamento = Id_Departamento;
        this.imagen=image;
        this.cantidad=cantidad;
    }
    public Producto(int id,int Precio, int Existencia, String Descripcion, int Id_Departamento, byte image[]) {
        this.id= id;
        this.Precio = Precio;
        this.Existencia = Existencia;
        this.Descripcion = Descripcion;
        this.Id_Departamento = Id_Departamento;
        this.imagen=image;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }
    

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    public int getPrecio() {
        return Precio;
    }

    public void setPrecio(int Precio) {
        this.Precio = Precio;
    }

    public int getExistencia() {
        return Existencia;
    }

    public void setExistencia(int Existencia) {
        this.Existencia = Existencia;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }
    

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public int getId_Departamento() {
        return Id_Departamento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public void setId_Departamento(int Id_Departamento) {
        this.Id_Departamento = Id_Departamento;
    }

    public boolean registrarProducto() throws IOException{
        MySqlConn objConn = new MySqlConn();
        try {
            InputStream is = this.part.getInputStream();
            objConn.PrepareStmt("INSERT INTO Producto values (?,?,?,?,?,?)"); //Se utiliza un PreparedStatement para evitar ataques por inyeccion SQL. mas info https://javabypatel.blogspot.com/2015/09/how-prepared-statement-in-java-prevents-sql-injection.html
            System.out.println("Selected");
            objConn.pstmt.setInt(1, 0);
            objConn.pstmt.setInt(2, this.Precio);
            objConn.pstmt.setInt(3, this.Existencia);
            objConn.pstmt.setString(4, this.Descripcion);
            objConn.pstmt.setInt(5, this.Id_Departamento);
            objConn.pstmt.setBlob(6, is);
            System.out.println("Replaced");
            objConn.pstmt.execute();
            System.out.println("No error");
            objConn.closeRsStmt();
            objConn.closeConnection();
            return true;

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeRsStmt();
        objConn.closeConnection();
        return false;
    }
       public static LinkedList getProductos(int dep){
    MySqlConn objConn = new MySqlConn();
        LinkedList <Producto> productos = new LinkedList();
        
        try {
            if(dep==0){
                objConn.PrepareStmt("SELECT * FROM Producto"); //Se utiliza un PreparedStatement para evitar ataques por inyeccion SQL. mas info https://javabypatel.blogspot.com/2015/09/how-prepared-statement-in-java-prevents-sql-injection.html
            }
            else{
                objConn.PrepareStmt("SELECT * FROM Producto WHERE Id_Departamento_FK=?");
                objConn.pstmt.setInt(1,dep);
            }
            
            //System.out.println("Selected");
            //System.out.println("Replaced");
            if (objConn.pstmt.execute()) {
                
                objConn.rs = objConn.pstmt.getResultSet();
                //objConn.rs.first();
                while(objConn.rs.next()){
                    productos.add(new Producto(objConn.rs.getInt(1),objConn.rs.getInt(2),objConn.rs.getInt(3),objConn.rs.getString(4),objConn.rs.getInt(5),objConn.rs.getBytes(6)));
                    
                }
                //for (int i = 0; i< depas.size(); i++){
                //    System.out.println(depas.get(i).toString());
                //}
                objConn.closeRsStmt();
                objConn.closeConnection();
                return productos;
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeRsStmt();
        objConn.closeConnection();
        return null;
    }
    public static boolean eliminarProducto(int id){
          MySqlConn objConn = new MySqlConn();
        
        try {
            
                objConn.PrepareStmt("DELETE FROM Producto WHERE Id_Producto=?");
                objConn.pstmt.setInt(1,id);
            
            
            //System.out.println("Selected");
            //System.out.println("Replaced");
            if (objConn.pstmt.execute()) {
                
            }
            return true;
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeRsStmt();
        objConn.closeConnection();
        return false;
    }
    public static Producto obtenerProducto(int id, int cantidad){
       MySqlConn objConn = new MySqlConn();
        try {
            Producto product = null;
            //Blob imagen;
            //Part pr;
            objConn.PrepareStmt("SELECT * FROM Producto Where Id_Producto = ?"); //Se utiliza un PreparedStatement para evitar ataques por inyeccion SQL. mas info https://javabypatel.blogspot.com/2015/09/how-prepared-statement-in-java-prevents-sql-injection.html
            
            //System.out.println("Selected");
            objConn.pstmt.setInt(1, id);
            //System.out.println("Id Profucto="+id);
            //System.out.println("Replaced");
            if (objConn.Ejecutar()){
               // System.out.println("No error");
                if (objConn.rs.next()){
                    
                    //pr=(Part)objConn.rs.getBinaryStream(6);
                    
                    product=new Producto(objConn.rs.getInt(1),objConn.rs.getInt(2), objConn.rs.getInt(3),objConn.rs.getString(4), objConn.rs.getInt(5), objConn.rs.getBytes(6), cantidad);    
                }
                
            }
            objConn.closeRsStmt();
            objConn.closeConnection();
            return product;

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeRsStmt();
        objConn.closeConnection();
        return null;
    }
    @Override
    public String toString() {
        return Descripcion+" "+Precio+" "+Existencia+" "+Id_Departamento;
    }
    
   
}
