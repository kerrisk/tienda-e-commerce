/*
 * Copyright (C) 2018 Erik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package miClase;

import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Erik
 */
public class PreguntasSeguridad {
    private int id;
    private String Respuesta;

    public PreguntasSeguridad(int id, String Respuesta) {
        this.id = id;
        this.Respuesta = Respuesta;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRespuesta() {
        return Respuesta;
    }

    public void setRespuesta(String Respuesta) {
        this.Respuesta = Respuesta;
    }

    @Override
    public String toString() {
        return "PreguntasSeguridad{" + "id=" + id + ", Respuesta=" + Respuesta + '}';
    }
        public static LinkedList getPreguntas(){
    MySqlConn objConn = new MySqlConn();
        LinkedList <PreguntasSeguridad> preguntas = new LinkedList();
        try {
            objConn.PrepareStmt("SELECT * FROM PreguntaRecuperacion"); //Se utiliza un PreparedStatement para evitar ataques por inyeccion SQL. mas info https://javabypatel.blogspot.com/2015/09/how-prepared-statement-in-java-prevents-sql-injection.html
            //System.out.println("Selected");
            //System.out.println("Replaced");
            if (objConn.pstmt.execute()) {
                
                objConn.rs = objConn.pstmt.getResultSet();
                //objConn.rs.first();
                while(objConn.rs.next()){
                    preguntas.add(new PreguntasSeguridad(objConn.rs.getInt(1),objConn.rs.getString(2)));
                    
                }
                
                objConn.closeRsStmt();
                objConn.closeConnection();
                return preguntas;
            }
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        objConn.closeRsStmt();
        objConn.closeConnection();
        return null;
}
}