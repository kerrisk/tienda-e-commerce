/*
 * Copyright (C) 2018 erikgoh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package miClase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

/**
 *
 * @author erikgoh
 */
public class MySqlConn {

    public PreparedStatement pstmt = null;
    public Statement stmt = null;
    public ResultSet rs = null;
    public Connection conn = null;

    public MySqlConn() {
        //Conectar con mysql...
        try {
            //cargando el driver
            Class.forName("org.mariadb.jdbc.Driver");
            /*String connectionUrl =
                            "jdbc:mariadb://nextcloudinstance.cqdo6ekpytv6.us-east-2.rds.amazonaws.com:3306/AbarrotesPedregal"
                            +"user=root&password=tC6WzwK2TYiSNtS6FWxr5kg";*/
            // conexion con la bd
            conn = DriverManager.getConnection("jdbc:mariadb://nextcloudinstance.cqdo6ekpytv6.us-east-2.rds.amazonaws.com:3306/FinalWeb", "root", "tC6WzwK2TYiSNtS6FWxr5kg");
        } catch (SQLException e) {
            System.out.println("SQL Exception: " + e.toString());
        } catch (ClassNotFoundException cE) {
            System.out.println("Class Not Found Exception: "
                    + cE.toString());
        }
    }

    public void PrepareStmt(String pquery) {
        try {
            pstmt = conn.prepareStatement(pquery);

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
    }

    public void Consult(String query) {
        //information_schema
        try {
            stmt = conn.createStatement();
            stmt.execute(query); //envia una consulta devuelve un objeto ResultSet para su implementacion
            rs = stmt.getResultSet(); //obtiene los resultados
            //se coloca sobre el primer registro
            rs.first();
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
    }

    public boolean Ejecutar() {
        try {
            pstmt.execute();
            rs = pstmt.getResultSet();
            rs.beforeFirst();
            if (rs.isBeforeFirst()){
                return true;
            }
            else 
                return false;
        } catch (SQLException ex) {
            System.out.println("Error en Ejecucion de PSTMT");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        return false;
    }

    public int Update(String query) {
        //information_schema
        int rModif = 0;
        try {
            stmt = conn.createStatement();
            rModif = stmt.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Error: " + ex.getErrorCode());
        }
        return rModif;
    }

    public void closeRsStmt() {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            } // ignore
            rs = null;
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException sqlEx) {
            } // ignore
            stmt = null;
        }
         if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException sqlEx) {
            } // ignore
            pstmt = null;
        }
    }

    public void closeConnection() {
        try {
            closeRsStmt();
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException sqlEx) {
        } // ignore
    }

    public String RecuperarTabla() {
        String tabla = "";
        if (this.rs != null) {
            tabla += "<table class=\"table table-hover\">";
            tabla += "    <thead>";
            tabla += "        <tr>";
            try {
                for (int j = 1; j <= this.rs.getMetaData().getColumnCount(); j++) {
                    tabla += "        <th scope=\"col\">" + this.rs.getMetaData().getColumnName(j) + "</th>";
                }
                tabla += "            </tr>";
                tabla += "        </thead>";
                tabla += "        <tbody>";
                this.rs.beforeFirst();
                while (this.rs.next()) {
                    tabla += "<tr>";
                    for (int j = 1; j <= this.rs.getMetaData().getColumnCount(); j++) {
                        tabla += "<td >" + this.rs.getString(j) + "</td>";
                    }
                    tabla += "</tr>";
                }// fin while%>
                tabla += "</tbody>";
            } catch (SQLException sqlEx) {
                System.out.println("SQLException: " + sqlEx.getMessage());
                System.out.println("SQLState: " + sqlEx.getSQLState());
                System.out.println("Error: " + sqlEx.getErrorCode());
            }
            tabla += "</table>";
        } else {
            tabla += "<h1>Error en la consulta</h1>";
        }
        return tabla;
    }
}
