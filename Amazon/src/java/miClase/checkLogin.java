/* 
 * Copyright (C) 2018 erikgoh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package miClase;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import javax.json.Json;
import javax.json.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author erikgoh
 */
@WebServlet(name = "checkLogin", urlPatterns = {"/checkLogin"})
public class checkLogin extends HttpServlet {
    
    Usuario usuario;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            usuario = new Usuario("","");
            JsonObject respuesta = null;
            boolean loggedin = false;
            /* TODO output your page here. You may use following sample code. */
            String paginaPrevia="/index.jsp";
            if (request.getParameter("paginaPrevia") != null){
                paginaPrevia = request.getParameter("paginaPrevia");
            }
        
            if (request.getParameter("User") != null) {
                usuario.setUsuario(request.getParameter("User"));
            }
            if (request.getParameter("Password") != null) {
                usuario.setPassword(request.getParameter("Password"));
            }
            //out.println(usuario.getUsuario());
            usuario.setPassword(Usuario.hashPassword(usuario.getPassword()));
            int resultadoLog=usuario.checkPassword();
            if (resultadoLog==1){
                HttpSession sesionOk = request.getSession();
                sesionOk.setAttribute("User", usuario);
                loggedin = true;
                respuesta  = Json.createObjectBuilder().add("loggedIn", loggedin).add("username",usuario.getUsuario()).add("password", usuario.getPassword()).build();
            }
            else if (resultadoLog==0){
                respuesta  = Json.createObjectBuilder().add("loggedIn", loggedin).add("username",usuario.getUsuario()).add("password", usuario.getPassword()).add("Error", "Usuario no registrado.").build();
            }
            else if (resultadoLog==-1){
                respuesta  = Json.createObjectBuilder().add("loggedIn", loggedin).add("username",usuario.getUsuario()).add("password", usuario.getPassword()).add("Error", "Error consultando la base de datos.").build();
            }
            else if (resultadoLog==-2){
                respuesta  = Json.createObjectBuilder().add("loggedIn", loggedin).add("username",usuario.getUsuario()).add("password", usuario.getPassword()).add("Error", "Tu cuenta esta bloqueada. Para recuperarla en el menu de inicio de sesion, da clic en recuperar contraseña.").build();
            }
            else if (resultadoLog==-4){
                respuesta  = Json.createObjectBuilder().add("loggedIn", loggedin).add("username",usuario.getUsuario()).add("password", usuario.getPassword()).add("Error", "Contraseña Incorrecta.").build();
            }
            
            out.print(respuesta);
            
            //getServletContext().getRequestDispatcher(paginaPrevia).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
