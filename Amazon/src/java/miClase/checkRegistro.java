/*
 * Copyright (C) 2018 erikgoh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package miClase;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author erikgoh
 */
@WebServlet(name = "checkRegistro", urlPatterns = {"/checkRegistro"})
public class checkRegistro extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            boolean loggedin = false;
            JsonObject respuesta;
//            Enumeration<String> params = request.getParameterNames();
//            while (params.hasMoreElements()) {
//                String paramName = params.nextElement();
//                System.out.println("Parameter Name - " + paramName + ", Value - " + request.getParameter(paramName));
//            }
            Usuario usuario = new Usuario(request.getParameter("UserR"),
                    request.getParameter("PasswordR"),
                    request.getParameter("Estado"),
                    request.getParameter("Calle"),
                    request.getParameter("Nombre"),
                    request.getParameter("CP"),
                    request.getParameter("email"),
                    request.getParameter("respPreg"),
                    Integer.parseInt(request.getParameter("idPreg"))
            );
            System.out.println(usuario.toString());
            usuario.setPassword(Usuario.hashPassword(usuario.getPassword())); // En esta parte es donde se Encripta la contraseña con 
            // el metodo HashPassword
            if (usuario.registrarUsuario()) {
                HttpSession sesionOk = request.getSession();
                sesionOk.setAttribute("User", usuario);
                loggedin = true;
            }
            respuesta = Json.createObjectBuilder().add("loggedIn", loggedin).add("username", usuario.getUsuario()).build();
            out.print(respuesta);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
