/*
 * Copyright (C) 2018 Erik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package miClase;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Erik
 */
@WebServlet(name = "ProcesarCompra", urlPatterns = {"/ProcesarCompra"})
public class ProcesarCompra extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Email email;
           email = new Email();
            Carrito carrito = (Carrito) request.getSession().getAttribute("carrito");
            Producto product_temp;
            Usuario usuario = (Usuario) request.getSession().getAttribute("User");
            String Salida = "";
            double subtotal = 0;
            Salida += "<div><br>";
            Salida += "<H1>Nota de Venta</H1><br>";
            Salida += "<ul><br>";
            for (int i = 0; i < carrito.getArticulos().size(); i++) {
                product_temp = (Producto) carrito.getArticulos().get(i);

                Salida += "<li>Producto: " + product_temp.getDescripcion() + " Precio: " + product_temp.getPrecio() + " Cantidad: " + product_temp.getCantidad() + " Subtotal: " + product_temp.getPrecio() * product_temp.getCantidad() + "</li><br>";
            }
            Salida += "</ul><br>";
            Salida += "<H2>Direccion de Envio</H2><br>";
            Salida += "Calle: " + request.getParameter("direccion") + "  Ciudad: " + request.getParameter("ciudad") + " Codigo Postal: " + request.getParameter("cp")
                    + " Numero de Telefono: " + request.getParameter("telcel");
            Salida += "<h2>Metodo de pago</h2><br>";
            Salida += request.getParameter("formaPago");
            Salida += "<h2>Impuestos</h2><br>";
            if (Integer.parseInt(request.getParameter("Id_Pais")) == 0) {
                subtotal = carrito.getSubtotal() * .16;
                Salida += "$" + subtotal;
            } else {
                subtotal = carrito.getSubtotal() * .20;
                Salida += "$$" + subtotal;
            }
            Salida += "<h2>Gastos de Envio</h2><br>";
            Salida += "$"+carrito.getGastosEnvio();
            Salida += "<h1>Total</h1><br>";
            Salida += "$"+(subtotal+carrito.getGastosEnvio());
            request.getSession().setAttribute("NotaVenta", Salida);
            getServletContext().getRequestDispatcher("/NotaVenta.jsp").forward(request, response);
            Salida += "</div><br>";
            
            email.usuarioNotaVenta(usuario.getNombre(), usuario.getEmail(),Salida);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
