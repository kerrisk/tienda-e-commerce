/*
 * Copyright (C) 2018 Erik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package miClase;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Erik
 */
@WebServlet(name = "ServletChat", urlPatterns = {"/ServletChat"})
public class ServletChat extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        Random rand = new Random();
        JsonObject respuesta = null;
        String msg = request.getParameter("msg");
        Usuario usuario = (Usuario)request.getSession().getAttribute("User");
        String sala = (String) getServletContext().getAttribute("sala");
        if (sala == null) {
            sala = "";
        }
        if (request.getSession().getAttribute("color") == null) {
            // Java 'Color' class takes 3 floats, from 0 to 1.
            int max = 255;
            int min = 0;
            int r = rand.nextInt((max - min) + 1) + min;
            int g = rand.nextInt((max - min) + 1) + min;
            int b = rand.nextInt((max - min) + 1) + min;
            String colorA = "rgb("+r+","+g+","+b+");";
            request.getSession().setAttribute("color", colorA);
        }

        if ((msg != null) && (msg.trim().length() > 0)) {
            HttpSession session = request.getSession();
            String nick = usuario.getNombre();
            String color = (String) session.getAttribute("color");

            sala += "<span style=\"color:" + color + "\">"
                    + nick + "</span>" + ": "
                    + msg + "<br/>";
        }

        getServletContext().setAttribute("sala", sala);
        respuesta  = Json.createObjectBuilder().add("send", "Mensaje Enviado").build();
        
        out.print(respuesta);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
